'use strict';

function createPoints(table) {
	table.increments('id').primary();
	table.string('point');
	table.integer('instalments_id').references('instalments.id');

	table.timestamps();
}

exports.up = function(knex, Promise) {
	return Promise.all([
		knex.schema.createTable('points', createPoints),
		]);
};

exports.down = function(knex, Promise) {
	return Promise.all([
		knex.schema.dropTable('points'),
		]);
};
