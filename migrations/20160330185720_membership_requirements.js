'use strict';

function createMR(table) {
    table.increments('id').primary();
    table.string('title');
    table.string('list');
    table.integer('travel_id').references('travels.id');
    
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('membership_requirements', createMR),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('membership_requirements'),
    ]);
};