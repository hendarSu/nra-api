'use strict';

function createRequirements(table) {
	table.increments('id').primary();
	table.integer('member_type');
	table.string('ktp');
	table.string('npwp');
	table.string('kk');
	table.string('buku_nikah');
	table.string('skk');
	table.string('legalitas_usaha');
	table.string('slip_gaji');
	table.string('laporan_keuangan');
	table.string('rekening_koran');
	table.integer('instalments_id').references('instalments.id');

	table.timestamps();
}

exports.up = function(knex, Promise) {
	return Promise.all([
		knex.schema.createTable('requirements', createRequirements),
		]);
};

exports.down = function(knex, Promise) {
	return Promise.all([
		knex.schema.dropTable('requirements'),
		]);
};
