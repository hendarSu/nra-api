'use strict';

function createSponsor(table) {
    table.increments('id').primary();
    table.bigInteger('premi');
    table.bigInteger('administration_price');
    table.integer('member_id').references('members.id');
    table.integer('sponsor_id').references('members.id');

    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('sponsors', createSponsor),
    ]);

};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('sponsors'),
    ]);
};
