'use strict';

function createNews(table) {
    table.increments('id').primary();

    table.string('title');
    table.string('realease');
    table.string('author');    
    table.string('source');
    table.string('link');
    table.string('image');
    table.string('category');
    table.integer('travel_id').references('travels.id');
    
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('news', createNews),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('news'),
    ]);
};