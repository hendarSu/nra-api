'use strict';

function createMd(table) {
    table.increments('id').primary();
    table.string('title');
    table.string('list');
    table.integer('travel_id').references('travels.id');
    
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('membership_description', createMd),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('membership_description'),
    ]);
};