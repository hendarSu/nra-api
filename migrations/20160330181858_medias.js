'use strict';

function createMedias(table) {
    table.increments('id').primary();
    table.string('name');
    table.string('url');
    table.integer('album_id').references('albums.id');
    table.string('type');

    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('medias', createMedias),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('medias'),
    ]);
};