'use strict';

function createBanks(table) {
    table.increments('id').primary();

    table.string('bankName');
    table.string('bankAccountName');
    table.string('bankAccountNumber');    
    table.string('currencyFormat');
    table.integer('travel_id').references('travels.id');
    
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('banks', createBanks),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('banks'),
    ]);
};