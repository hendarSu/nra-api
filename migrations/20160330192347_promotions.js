
'use strict';

function createPromotions(table) {
	table.increments('id').primary();
	table.string('title');
	table.string('descriptions');
	table.string('price');
	table.string('type');
	table.date('release');
	table.date('start');
	table.date('end');
	table.integer('travel_id').references('travels.id');

	table.timestamps();
}

exports.up = function(knex, Promise) {
	return Promise.all([
		knex.schema.createTable('promotions', createPromotions),
		]);
};

exports.down = function(knex, Promise) {
	return Promise.all([
		knex.schema.dropTable('promotions'),
		]);
};
