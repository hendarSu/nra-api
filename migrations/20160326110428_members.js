'use strict';

function createMembers(table) {
    table.increments('id').primary();
    table.string('member_id').unique().notNullable();
    table.string('fullname');
    table.string('address');
    table.string('born_place');
    table.date('birth_date');
    table.string('sex');
    table.string('street');
    table.string('rt');
    table.string('rw');
    table.string('subdistrict');
    table.string('town');
    table.string('province');
    table.string('postal_code');
    table.string('telephone');
    table.string('telephone_alt');
    table.string('faximile');
    table.string('handphone');
    table.string('handphone_alt');
    table.integer('travel_id').references('travels.id').onDelete('CASCADE');;

    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('members', createMembers),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('members'),
    ]);
};
