'use strict';

function createTAC(table) {
    table.increments('id').primary();
    table.string('title');
    table.string('list');
    table.integer('travel_id').references('travels.id');
    
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('terms_and_condition', createTAC),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('terms_and_condition'),
    ]);
};