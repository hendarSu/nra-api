
'use strict';

function createPromotionsDepart(table) {
	table.increments('id').primary();
	table.date('date');
	table.integer('promotion_id').references('promotions.id');

	table.timestamps();
}

exports.up = function(knex, Promise) {
	return Promise.all([
		knex.schema.createTable('promotions_depart', createPromotionsDepart),
		]);
};

exports.down = function(knex, Promise) {
	return Promise.all([
		knex.schema.dropTable('promotions_depart'),
		]);
};
