'use strict';

function createApproval(table) {
    table.increments('id').primary();
    table.string('status');
    table.string('place');
    table.date('date');
    table.integer('member_id').references('members.id');
    table.integer('employe_id').references('employes.id');

    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('approval', createApproval),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('approval'),
    ]);
};
