'use strict';

function createDetails(table) {
	table.increments('id').primary();
	table.integer('nominal');
	table.integer('one_years');
	table.integer('two_years');
	table.integer('instalments_id').references('instalments.id');

	table.timestamps();
}

exports.up = function(knex, Promise) {
	return Promise.all([
		knex.schema.createTable('details', createDetails),
		]);
};

exports.down = function(knex, Promise) {
	return Promise.all([
		knex.schema.dropTable('details'),
		]);
};
