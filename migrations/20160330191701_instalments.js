'use strict';

function createInstalments(table) {
	table.increments('id').primary();
	table.string('title');
	table.string('sub_title');
	table.integer('travel_id').references('travels.id');

	table.timestamps();
}

exports.up = function(knex, Promise) {
	return Promise.all([
		knex.schema.createTable('instalments', createInstalments),
		]);
};

exports.down = function(knex, Promise) {
	return Promise.all([
		knex.schema.dropTable('instalments'),
		]);
};