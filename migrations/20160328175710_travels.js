'use strict';

function createTravels(table) {
    table.increments('id').primary();
    table.string('name');
    table.string('company');
    table.string('slogan');
    table.date('address');
    table.string('sex');
    table.string('street');
    table.string('subdistrict');
    table.string('town');
    table.string('province');
    table.string('postal_code');
    table.string('faximile');

    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('travels', createTravels),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('travels'),
    ]);
};
