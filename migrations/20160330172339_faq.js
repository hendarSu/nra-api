'use strict';

function createFaq(table) {
    table.increments('id').primary();
    table.string('question');
    table.string('answer');
    table.integer('travel_id').references('travels.id');

    table.timestamps();
    
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('faq', createFaq),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('faq'),
    ]);
};