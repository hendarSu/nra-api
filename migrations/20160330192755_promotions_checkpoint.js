
'use strict';

function createPromotionscheckPoint(table) {
	table.increments('id').primary();
	table.string('checkpoint');
	table.integer('promotion_id').references('promotions.id');

	table.timestamps();
}

exports.up = function(knex, Promise) {
	return Promise.all([
		knex.schema.createTable('promotions_checkpoint', createPromotionscheckPoint),
		]);
};

exports.down = function(knex, Promise) {
	return Promise.all([
		knex.schema.dropTable('promotions_checkpoint'),
		]);
};
