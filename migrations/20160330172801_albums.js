'use strict';

function createAlbums(table) {
    table.increments('id').primary();
    table.string('name');
    table.date('date');
    table.string('description');
    table.string('password');
    table.integer('travel_id').references('travels.id');

    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        // knex.schema.createTable('albums', createAlbums),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        // knex.schema.dropTable('albums'),
    ]);
};