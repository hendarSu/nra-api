'use strict';

function createPhone(table) {
    table.increments('id').primary();
    table.string('phone_number');
    table.integer('travel_id').references('travels.id');

    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('phone_number', createPhone),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('phone_number'),
    ]);
};