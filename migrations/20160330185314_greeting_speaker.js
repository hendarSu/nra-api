'use strict';

function createGS(table) {
    table.increments('id').primary();
    table.string('speaker');
    table.string('position_on_company');
    table.string('information_opening');
    table.integer('travel_id').references('travels.id');
    
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('greeting_speaker', createGS),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('greeting_speaker'),
    ]);
};