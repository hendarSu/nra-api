'use strict';

function createHP(table) {
    table.increments('id').primary();
    table.string('handphone_number');
    table.integer('travel_id').references('travels.id');

    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('handphone_number', createHP),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('handphone_number'),
    ]);
};