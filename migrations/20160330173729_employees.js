'use strict';

function createEmployes(table) {
    table.increments('id').primary();
    table.string('fullname');
    table.string('email');
    table.string('password');

    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('employes', createEmployes),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('employes'),
    ]);
};
