'use strict';

function createMB(table) {
    table.increments('id').primary();
    table.string('title');
    table.string('list');
    table.integer('travel_id').references('travels.id');
    
    table.timestamps();
}

exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('membership_benefits', createMB),
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('membership_benefits'),
    ]);
};