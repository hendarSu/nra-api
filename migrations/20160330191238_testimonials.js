'use strict';

function createTestimonials(table) {
	table.increments('id').primary();
	table.string('name');
	table.string('email');
	table.string('comment');
	table.date('date');
	table.string('status');
	table.integer('travel_id').references('travels.id');

	table.timestamps();
}

exports.up = function(knex, Promise) {
	return Promise.all([
		knex.schema.createTable('testimonials', createTestimonials),
		]);
};

exports.down = function(knex, Promise) {
	return Promise.all([
		knex.schema.dropTable('testimonials'),
		]);
};