'use strict';

const Token = require('../models/Token');

module.exports = bookshelf.Collection.extend({
    model: Token,
});