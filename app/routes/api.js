'use strict';

const express = require('express');
const router = express.Router();
const auth = require('../middlewares/authorized');

router.get('/', auth.isAuthorized(), (req, res, next) => {
    res.ok('Node API. Maybe');
});

module.exports = router;
