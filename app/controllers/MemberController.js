'use strict';

module.exports = {
    get: Async.route(function *(req, res, next) {
        const model = res.model;
        res.ok(model);
    }),

};

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(v, i) {
        cr(v, i).catch(console.error);
    };
}
