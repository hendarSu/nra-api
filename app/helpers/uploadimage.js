'use strict';

const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'));
const path = require('path');
const s3 = new AWS.S3({ params: { Bucket: config.aws.s3.bucket } });

function makeUploader(Body, name) {
    const params = {
        Bucket: config.aws.s3.bucket,
        Key: name,
        ACL: 'public-read',
        Body,
    };

    const managedUpload = new AWS.S3.ManagedUpload({ params });

    return Promise.promisifyAll(managedUpload, {suffix: 'Bsync'});
}

function removeObject(key) {
    const params = {
        Bucket: config.aws.s3.bucket,
        Key: key,

        // MFA: 'STRING_VALUE',
        RequestPayer: 'requester',
    };

    const aws3 = new AWS.S3();
    const resDelete = aws3.deleteObject(params);// successful response

    return Promise.promisifyAll(resDelete, {suffix: 'Bsync'});
}

module.exports = {
    fromFile: Async.make(function *(prefix, file, write) {
        if (file.type.indexOf('image') !== 0) {
            throw error('Not a valid image type', 400);
        }

        const splittedName = file.name.split('.');
        const extension = splittedName[splittedName.length - 1];
        const withoutExt = `${prefix}_${splittedName.slice(0, splittedName.length - 1).join('')}_${Date.now()}`;
        const newName = `${withoutExt}.${extension}`;
        const newPath = path.join(config.uploadDir, newName);

        if (write) {
            const writeStream = fs.createWriteStream(newPath);
            const readStream = fs.createReadStream(file.path);

            readStream.pipe(writeStream);
        }

        const Body = fs.createReadStream(file.path);
        const uploadResult = yield makeUploader(Body, newName).sendBsync();

        yield fs.unlinkAsync(file.path);
        return uploadResult.Location;
    }),

    base64: Async.make(function *(prefix, base64string) {
        const validationRe = /^data:image\/(png|jpeg);base64,/;

        if (!validationRe.test(base64string)) throw error('Not a valid image type', 400);

        const withoutExt = `${prefix}_${Utils.uid(32)}_${Date.now()}`;
        const newFilename = `${withoutExt}.png`;
        const newBase64String = base64string.replace(/^data:image\/(png|jpeg);base64,/, '');
        const newPath = path.join(config.uploadDir, newFilename);

        yield fs.writeFileAsync(newPath, newBase64String, 'base64').catch(console.error);

        try {
            const stats = yield fs.statAsync(newPath);
        } catch (ex) {
            console.error(ex);
            return false;
        }

        const Body = fs.createReadStream(newPath);
        const uploadResult = yield makeUploader(Body, newFilename).sendBsync();
        yield fs.unlinkAsync(newPath);

        return uploadResult.Location;
    }),

    remove: Async.make(function *(key) {
        console.log(key)
        const removeResult = yield removeObject(key).sendBsync();
        console.log(removeResult);
        return removeResult;
    }),
};
