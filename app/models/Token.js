'use strict';

module.exports = bookshelf.Model.extend({
    tableName: 'tokens',
    hasTimestamps: true,
});
