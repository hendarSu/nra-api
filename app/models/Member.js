'use strict';

module.exports = bookshelf.Model.extend({
    tableName: 'members',
    hasTimestamps: true,
});
